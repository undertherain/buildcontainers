import numpy as np
import chainer

cnt_channels = 3
kernel_size = 3
batch_size = 32
image_size = 224
stride = 1
pad = 0
out_channels = 32
conv = chainer.links.Convolution2D(
    in_channels=cnt_channels,
    out_channels=out_channels,
    ksize=kernel_size,
    stride=stride,
    pad=pad,
    nobias=False,
    initialW=None,
    initial_bias=None,
    dilate=1,
    groups=1)

images = np.random.random((batch_size, cnt_channels, image_size, image_size)).astype(np.float32)

cols = chainer.functions.im2col(images, ksize=kernel_size)
size_out = 1 + ((image_size - kernel_size + 2 * pad) // stride)
print("expected shape", (batch_size, kernel_size * kernel_size * cnt_channels, size_out, size_out))
print("shape of cols", cols.shape)

dims_flattened = (batch_size * size_out * size_out,
                  kernel_size * kernel_size * cnt_channels,
                  out_channels)
print("expected gemm", dims_flattened)
res = conv(images)
print(res.shape)
