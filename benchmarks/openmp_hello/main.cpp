#include <iostream>
#include <omp.h>


int main(int argc, char *argv[]) {
    std::cerr << "max threads:" << omp_get_max_threads() << std::endl;
    #pragma omp parallel 
    {
       int id = omp_get_thread_num();
	   std::cerr << "hi #" << id << std::endl;
    }
}