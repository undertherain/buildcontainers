#include <sys/time.h>
#include "darknet.h"

// typedef struct matrix{
//     int rows, cols;
//     float **vals;
// } matrix;

// typedef struct{
//     int w, h;
//     matrix X;
//     matrix y;
//     int shallow;
//     int *num_boxes;
//     box **boxes;
// } data;

// X.raws = samples
// X.cols = w * h * c

data get_resnet_data(size_t cnt_samples, size_t cnt_pixels)
{
    data res;
    matrix X;
    X.rows = cnt_samples;
    X.cols = cnt_pixels;
    X.vals = calloc(X.rows, sizeof(float*));
    for (size_t i=0; i<X.rows; i++)
        X.vals[i] = calloc(X.cols, sizeof(float *));
    res.X = X;
    res.shallow = 0;
    return res;
}

void test_cifar(char *filename, char *weightfile, uint batch_size)
{
    network *net = load_network(filename, weightfile, 0);
    set_batch_network(net, batch_size);
    srand(time(0));
    uint cnt_samples = batch_size * 10;
    clock_t time;
    float avg_acc = 0;
    float avg_top5 = 0;
    data test = get_resnet_data(cnt_samples, 224 * 224 * 3);
    //load_cifar10_data("data/cifar-10-batches-bin/test_batch.bin");
    fprintf(stderr, "loaded data\n");
    fprintf(stderr, "w: %d h:%d shallow:%d\n", test.w, test.h, test.shallow);
    fprintf(stderr, "X rows: %d cols: %d\n", test.X.rows, test.X.cols);
    fprintf(stderr, "batch set to : %d\n", net->batch);

    struct timeval t1, t2;
    double elapsed_time;
    gettimeofday(&t1, NULL);
    fprintf(stderr, "predicting....\n");
    matrix pred = network_predict_data(net, test);
    // network_predict(net, test.X.vals[0]);
    gettimeofday(&t2, NULL);
    elapsed_time = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    elapsed_time += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
    printf("%f ms.\n", elapsed_time);
    double samples_per_sec = (float) cnt_samples / elapsed_time * 1000;
    printf("ips: %f\n", samples_per_sec);
    
    free_data(test);
    return;
    float *acc = network_accuracies(net, test, 2);
    avg_acc += acc[0];
    avg_top5 += acc[1];
    printf("top1: %f, %lf seconds, %d images\n", avg_acc, sec(clock()-time), test.X.rows);
}

void main(int argc, char **argv)
{
    // char *cfg = "cfg/cifar.test.cfg";
    char *cfg = "cfg/resnet50.cfg";
    char *weights = 0;
    uint batch_size = 32;
    if (argc > 1)
        batch_size = atoi(argv[1]);
    test_cifar(cfg, weights, batch_size);
}


