    #include <algorithm>
#include <cmath>
#include <chrono>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>

#include "example_utils.hpp"
#include "oneapi/dnnl/dnnl.hpp"
#include "argparse.hpp"

using namespace dnnl;

using tag = memory::format_tag;
using dt = memory::data_type;

double do_convolution(dnnl::engine::kind engine_kind,
                      const size_t cnt_iterations,
                      const memory::dim batch_size,
                      memory::dim in_channels,
                      size_t input_size,
                      memory::dim out_channels,
                      size_t kernel_size)
                       {

    dnnl::engine engine(engine_kind, 0);
    dnnl::stream engine_stream(engine);

    // Tensor dimensions.
    const memory::dim in_H = input_size, // input height
            in_W = input_size, // input width
            kern_H = kernel_size, // weights height
            kern_W = kernel_size, // weights width
            pad_H_L = 1, // height padding: left
            pad_H_R = 1, // height padding: right
            pad_W_L = 1, // width padding: left
            pad_W_R = 1, // width padding: right
            stride_H = 1, // height-wise stride
            stride_W = 1, // width-wise stride
            out_H = (in_H - kern_H + pad_H_L + pad_H_R) / stride_H + 1, // output height
            out_W = (in_W - kern_W + pad_W_L + pad_W_R) / stride_W + 1; // output width

    // Source (src), weights, bias, and destination (dst) tensors
    // dimensions.
    memory::dims src_dims = {batch_size, in_channels, in_H, in_W};
    memory::dims weights_dims = {out_channels, in_channels, kern_H, kern_W};
    memory::dims bias_dims = {out_channels};
    memory::dims dst_dims = {batch_size, out_channels, out_H, out_W};

    // Strides, padding dimensions.
    memory::dims strides_dims = {stride_H, stride_W};
    memory::dims padding_dims_l = {pad_H_L, pad_W_L};
    memory::dims padding_dims_r = {pad_H_R, pad_W_R};

    // Allocate buffers.
    std::vector<float> src_data(product(src_dims));
    std::vector<float> weights_data(product(weights_dims));
    std::vector<float> bias_data(out_channels);
    std::vector<float> dst_data(product(dst_dims));

    // Initialize src, weights, and dst tensors.
    std::generate(src_data.begin(), src_data.end(), []() {
        static int i = 0;
        return std::cos(i++ / 10.f);
    });
    std::generate(weights_data.begin(), weights_data.end(), []() {
        static int i = 0;
        return std::sin(i++ * 2.f);
    });
    std::generate(bias_data.begin(), bias_data.end(), []() {
        static int i = 0;
        return std::tanh(i++);
    });

    // Create memory objects for tensor data (src, weights, dst). In this
    // example, NCHW layout is assumed for src and dst, and OIHW for weights.
    auto user_src_mem = memory({src_dims, dt::f32, tag::nchw}, engine);
    auto user_weights_mem = memory({weights_dims, dt::f32, tag::oihw}, engine);
    auto user_dst_mem = memory({dst_dims, dt::f32, tag::nchw}, engine);

    // Create memory descriptors with format_tag::any for the primitive. This
    // enables the convolution primitive to choose memory layouts for an
    // optimized primitive implementation, and these layouts may differ from the
    // ones provided by the user.
    auto conv_src_md = memory::desc(src_dims, dt::f32, tag::any);
    auto conv_weights_md = memory::desc(weights_dims, dt::f32, tag::any);
    auto conv_dst_md = memory::desc(dst_dims, dt::f32, tag::any);

    // Create memory descriptor and memory object for input bias.
    auto user_bias_md = memory::desc(bias_dims, dt::f32, tag::a);
    auto user_bias_mem = memory(user_bias_md, engine);

    // Write data to memory object's handle.
    write_to_dnnl_memory(src_data.data(), user_src_mem);
    write_to_dnnl_memory(weights_data.data(), user_weights_mem);
    write_to_dnnl_memory(bias_data.data(), user_bias_mem);

    // Create operation descriptor.
    std::cerr << "creating conv desc \n";
    auto conv_desc = convolution_forward::desc(prop_kind::forward_training,
            algorithm::convolution_direct, conv_src_md, conv_weights_md,
            user_bias_md, conv_dst_md, strides_dims, padding_dims_l,
            padding_dims_r);

    // Create primitive post-ops (ReLU).
    const float scale = 1.f;
    const float alpha = 0.f;
    const float beta = 0.f;
    post_ops conv_ops;
    conv_ops.append_eltwise(scale, algorithm::eltwise_relu, alpha, beta);
    primitive_attr conv_attr;
    conv_attr.set_post_ops(conv_ops);

    // Create primitive descriptor.
    auto conv_pd
            = convolution_forward::primitive_desc(conv_desc, conv_attr, engine);

    // For now, assume that the src, weights, and dst memory layouts generated
    // by the primitive and the ones provided by the user are identical.
    auto conv_src_mem = user_src_mem;
    auto conv_weights_mem = user_weights_mem;
    auto conv_dst_mem = user_dst_mem;

    // Reorder the data in case the src and weights memory layouts generated by
    // the primitive and the ones provided by the user are different. In this
    // case, we create additional memory objects with internal buffers that will
    // contain the reordered data. The data in dst will be reordered after the
    // convolution computation has finalized.
    if (conv_pd.src_desc() != user_src_mem.get_desc()) {
        std::cerr << "reorder src \n";
        conv_src_mem = memory(conv_pd.src_desc(), engine);
        reorder(user_src_mem, conv_src_mem)
                .execute(engine_stream, user_src_mem, conv_src_mem);
    }

    if (conv_pd.weights_desc() != user_weights_mem.get_desc()) {
        std::cerr << "reorder weights \n";
        conv_weights_mem = memory(conv_pd.weights_desc(), engine);
        reorder(user_weights_mem, conv_weights_mem)
                .execute(engine_stream, user_weights_mem, conv_weights_mem);
    }

    if (conv_pd.dst_desc() != user_dst_mem.get_desc()) {
        std::cerr << "dunno what with memoery \n";
        conv_dst_mem = memory(conv_pd.dst_desc(), engine);
    }

    // Create the primitive.
    auto conv_prim = convolution_forward(conv_pd);

    // Primitive arguments.
    std::unordered_map<int, memory> conv_args;
    conv_args.insert({DNNL_ARG_SRC, conv_src_mem});
    conv_args.insert({DNNL_ARG_WEIGHTS, conv_weights_mem});
    conv_args.insert({DNNL_ARG_BIAS, user_bias_mem});
    conv_args.insert({DNNL_ARG_DST, conv_dst_mem});

    // Primitive execution: convolution with ReLU.
    std::cerr << "executing the primitive \n";
    auto start = std::chrono::high_resolution_clock::now(); 
    for (size_t i=0; i<cnt_iterations; i++)
        conv_prim.execute(engine_stream, conv_args);
    auto stop = std::chrono::high_resolution_clock::now();

    // Reorder the data in case the dst memory descriptor generated by the
    // primitive and the one provided by the user are different.
    if (conv_pd.dst_desc() != user_dst_mem.get_desc()) {
        reorder(conv_dst_mem, user_dst_mem)
                .execute(engine_stream, conv_dst_mem, user_dst_mem);
    } else
        user_dst_mem = conv_dst_mem;

    // Wait for the computation to finalize.
    engine_stream.wait();

    // Read data from memory object's handle.
    read_from_dnnl_memory(dst_data.data(), user_dst_mem);
    std::chrono::duration<double> seconds = (stop - start); 
    double dtime = seconds.count();

    double ips = cnt_iterations * batch_size / dtime;
    double flop_estimated = 2 * out_W * out_H * in_channels * batch_size * out_channels * kern_W * kern_H * cnt_iterations;
    double gflop_estimated = flop_estimated / 1000000000.0;
    double glop_per_sec = gflop_estimated / dtime;
    std::cout << "time: " << dtime << std::endl;
    std::cout << "ips: " << ips << std::endl;
    std::cout << "GFLOPS/s: " << glop_per_sec << std::endl;

    return dtime;

}


int main(int argc, char *argv[]) {
    std::cerr << "starting... \n";
    std::cerr << "max threads:" << omp_get_max_threads() << std::endl;
    argparse::ArgumentParser argparser("DNNL benchmark");

    argparser.add_argument("--batch_size")
      .help("batch size")
      .default_value(32)
      .action([](const std::string& value) { return std::stoi(value); });

    argparser.add_argument("--cnt_iterations")
      .help("number of iterations")
      .default_value(1)
      .action([](const std::string& value) { return std::stoi(value); });

    argparser.add_argument("--in_channels")
      .help("input channels")
      .default_value(3)
      .action([](const std::string& value) { return std::stoi(value); });

    argparser.add_argument("--in_size")
      .help("input size (for both w and h)")
      .default_value(224)
      .action([](const std::string& value) { return std::stoi(value); });

    argparser.add_argument("--out_channels")
      .help("number of conv filters")
      .default_value(32)
      .action([](const std::string& value) { return std::stoi(value); });

    argparser.add_argument("--kernel_size")
      .help("size of conv filters")
      .default_value(3)
      .action([](const std::string& value) { return std::stoi(value); });

    try {
        argparser.parse_args(argc, argv);
    }
    catch (const std::runtime_error& err) {
        std::cout << err.what() << std::endl;
        std::cout << argparser;
        exit(0);
    }

    auto engine = validate_engine_kind(dnnl::engine::kind::cpu);
    // parse_engine_kind(argc, argv);
    auto kernel = do_convolution;
    // handle_example_errors(kernel, engine);
    double dtime = kernel(engine,
                          argparser.get<int>("--cnt_iterations"),
                          argparser.get<int>("--batch_size"),
                          argparser.get<int>("--in_channels"),
                          argparser.get<int>("--in_size"),
                          argparser.get<int>("--out_channels"),
                          argparser.get<int>("--kernel_size")
                          );
}
