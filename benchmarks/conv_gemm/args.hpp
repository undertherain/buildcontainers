// traditional BLAS API:
// M - Number of rows in matrices A and C.
// N - Number of columns in matrices B and C.
// K - Number of columns in matrix A; number of rows in matrix B.
// we remap from intuitive order to BLAS-style

struct Options {
    std::string precision;
    size_t cnt_channels;
    size_t size_image;
    size_t batch_size;
    size_t kernel_size;
    size_t out_channels;
    size_t stride;
    size_t iterations;
};

Options parse_args(const int argc, char *argv[]) {
    if (argc != 9)
    {
        std::cerr << "provide precision, batch, channels, size, out_channels, stride\n"; 
        std::cerr << "as command line parameters\n";
        std::cerr << "got " << argc << " parameters\n";
        exit(-1);
    }
    Options options;
    options.precision = std::string(argv[1]);
    options.batch_size = atoi(argv[2]);
    options.cnt_channels = atoi(argv[3]);
    options.size_image = atoi(argv[4]);
    options.kernel_size = atoi(argv[5]);
    options.out_channels = atoi(argv[6]);
    options.stride = atoi(argv[7]);
    options.iterations = atoi(argv[8]);
    return options;
}
