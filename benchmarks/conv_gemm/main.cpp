#include <iostream>
#include <chrono>
#include <cblas.h>
#include "args.hpp"
using namespace std::chrono; 

template<typename precision>
void get_matrices(size_t &cnt_rows_A_rows_C,
                  size_t &cnt_cols_A_rows_B,
                  size_t &cnt_cols_B_cols_C,
                  precision * & A,
                  precision * & B,
                  precision * & C) {
    size_t i;
    fprintf(stderr, "doing  malloc... ");
    A = (precision*) malloc(sizeof(precision) * cnt_rows_A_rows_C * cnt_cols_A_rows_B);
    B = (precision*) malloc(sizeof(precision) * cnt_cols_A_rows_B * cnt_cols_B_cols_C);
    C = (precision*) malloc(sizeof(precision) * cnt_rows_A_rows_C * cnt_cols_B_cols_C);
    fprintf(stderr, "done\ndoing rnd init... ");
    for(i=0; i < cnt_rows_A_rows_C * cnt_cols_A_rows_B; i++) {A[i] = static_cast<float>(rand()) / RAND_MAX;}
    for(i=0; i < cnt_cols_A_rows_B * cnt_cols_B_cols_C; i++) {B[i] = static_cast<float>(rand()) / RAND_MAX;}
    for(i=0; i < cnt_rows_A_rows_C * cnt_cols_B_cols_C; i++) {C[i] = static_cast<float>(rand()) / RAND_MAX;}
    fprintf(stderr, "done\n");
}

int main(int argc, char * argv[])
{
    std::cerr << "starting\n";
    Options options = parse_args(argc, argv);
    //int cnt_channels = 3;
    //int image_size = 224;
    //int batch_size = 32;
    //int kernel_size = 3;
    // int out_channels = 32;
    // int stride = 1;
    int pad = 0;
    
    int size_out = 1 + ((options.size_image - options.kernel_size + 2 * pad) / options.stride);

    int cnt_rows_A_rows_C = options.batch_size * size_out * size_out;
    int cnt_cols_A_rows_B = options.kernel_size * options.kernel_size * options.cnt_channels;
    int cnt_cols_B_cols_C = options.out_channels;
    float *A, *B, *C;
    double dtime;
    size_t m = cnt_rows_A_rows_C;
    size_t n = cnt_cols_A_rows_B;
    size_t k = cnt_cols_B_cols_C;
    std::cerr << "conv:\t" << options.batch_size<< "x" << options.cnt_channels << "x" <<
        options.size_image << "x" << options.size_image << "_" <<
        options.out_channels << "x" << options.kernel_size << "x" << options.kernel_size 
        << "_p" << pad << "_s" << options.stride
        << "_i" << options.iterations <<"\n";
    get_matrices<float>(m, k, n, A, B, C);
    const float alpha = 1;
    const float beta = 0;
    const size_t lda=m; // k for row major;
    const size_t ldb=k; //n; 
    const size_t ldc=m; //n;
    std::string precision = "FP32";
    std::cerr << "preheat... ";
    if (precision == "FP32")
        cblas_sgemm(CblasColMajor,
                    CblasNoTrans,
                    CblasNoTrans,
                    m,
                    n,
                    k,
                    alpha,
                    A, lda,
                    B, ldb,
                    beta,
                    C, ldc);
    else
    {
        std::cerr << "not implemented yet\n";
        throw "madamada";
    }    
    std::cerr << "done\n";
    auto start = high_resolution_clock::now(); 
    for (size_t i=0; i<options.iterations; i++)
    {
        if (precision == "FP32")
            cblas_sgemm(CblasColMajor,
                        CblasNoTrans,
                        CblasNoTrans,
                        m,
                        n,
                        k,
                        alpha,
                        A, lda,
                        B, ldb,
                        beta,
                        C, ldc);
        else
        {
            // TODO  (Alex): implement FP16
            // ugly throw here to make sure benchmarker chrashes alright
            std::cerr << "not implemented yet\n";
            throw "madamada";
        }
    }
    std::cerr << "GEMM size: " << m << " " << n << " " << k << std::endl;
    auto stop = high_resolution_clock::now();
    std::chrono::duration<double> seconds = (stop - start); 
    dtime = seconds.count();
    double gflop = (2.0 * m * n * k) / (1000 * 1000 * 1000);
    gflop *= static_cast<double>(options.iterations);
    double gflops = gflop / dtime;
    // printf("%f\n", dtime);
    double gflop_effective = (2.0 * size_out * size_out * options.cnt_channels * options.batch_size 
        * options.out_channels 
        * options.kernel_size * options.kernel_size 
        * options.iterations) / (1000000000L);
    std::cerr << "time:\t" << dtime << "\n";
    std::cerr << "ips:\t" << static_cast<double>(options.batch_size * options.iterations) / dtime << "\n";
    std::cerr << "gflops gemm:\t" << gflop << "\n";
    std::cerr << "gflops effective:\t" << gflop_effective << "\n";
    std::cerr << "gflops/s: \t" << gflops << "\n";

    return 0;
}
