# script for compiling OpenCV from sources

OPENCV_VERSION="4.5.1"
BUILD_DIR=/tmp/opencv
mkdir -p $BUILD_DIR
cd $BUILD_DIR
wget -nv -nc -O opencv_${OPENCV_VERSION}.zip https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip 
wget -nv -nc -O opencv_contrib_${OPENCV_VERSION}.zip https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.zip
rm -rf opencv-${OPENCV_VERSION} || true
rm -rf opencv_contrib-${OPENCV_VERSION} || true
rm -rf opencv || true
rm -rf opencv_contrib || true
unzip opencv_${OPENCV_VERSION}.zip
unzip opencv_contrib_${OPENCV_VERSION}.zip
mv opencv-${OPENCV_VERSION} opencv
mv opencv_contrib-${OPENCV_VERSION} opencv_contrib
cd ./opencv
mkdir build
cd build

# echo detected Python path as $(python3 -m site --user-site)
# CC=fcc CXX=FCC \
cmake -D CMAKE_BUILD_TYPE=RELEASE \
      -D BUILD_SHARED_LIBS=ON \
      -D BUILD_EXAMPLES=OFF \
      -D INSTALL_C_EXAMPLES=OFF \
      -D INSTALL_PYTHON_EXAMPLES=OFF \
      -D BUILD_opencv_java=OFF \
      -D BUILD_TESTS=OFF \
      -D BUILD_opencv_python=OFF \
      -D BUILD_opencv_python3=OFF \
      -D CMAKE_INSTALL_PREFIX=$HOME/opt/extra/opencv/gcc \
      -D OPENCV_SKIP_PYTHON_LOADER=ON \
      -D CV_DISABLE_OPTIMIZATION=ON \
      -D WITH_WEBP=OFF \
      -D WITH_TIFF=OFF \
      -D WITH_OPENJPEG=OFF \
      -D WITH_JASPER=OFF \
      -D WITH_OPENEXR=OFF \
      -D WITH_OPENCL=OFF \
      -D WITH_IPP=OFF \
      -D WITH_TBB=OFF \
      -D WITH_EIGEN=ON \
      ..

      # -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \

make -j 12
make install

cd ../.. 

#if [ -f /.dockerenv ]; then
#    echo "We are instide docker, cleaning downloads";
#    rm -rf opencv || true
#    rm -rf opencv_contrib || true
#    rm opencv_${OPENCV_VERSION}.zip
#    rm opencv_contrib_${OPENCV_VERSION}.zip
#else
#    echo "We are on real OS, keeping opencv archive";
#fi

    #  -DBUILD_TIFF=ON \
    #  -DWITH_CUDA=OFF \
    #  -DWITH_OPENGL=ON \
    #  -DWITH_V4L=ON \
    #  -DBUILD_PERF_TESTS=OFF \
