#include <iostream>
#include <vector>
#include <chrono>
#include <opencv2/dnn.hpp>
#include <opencv2/imgcodecs.hpp>
// #include <opencv2/imgproc.hpp>

using std::chrono::high_resolution_clock;
using cv::Mat;

int main(int argc, char const *argv[])
{
    cv::String info=cv::getBuildInformation();
    std::cerr << info << "\n";
    size_t batch_size = 32;
    size_t cnt_iterations = 10;
    if (argc > 1)
        batch_size = atoi(argv[1]);
    if (argc > 2)
	cnt_iterations = atoi(argv[2]);
    if (argc > 3)
    {
	size_t cnt_threads = atoi(argv[3]);
        cv::setNumThreads(cnt_threads);
    }
    std::cerr << "hi \n";
    cv::String model = "/home/ra000012/data/models/resnet50/resnet50.caffemodel";
    cv::String config = "/home/ra000012/data/models/resnet50/ResNet-50-deploy.prototxt";
    cv::String framework = "";
    cv::dnn::Net net = cv::dnn::readNet(model, config);
    // read or create iame
    cv::String filename = "cat.jpg";
    std::vector<Mat> images;
    for (size_t i=0; i<batch_size; i++){
        Mat image = cv::imread (filename, 3);
        images.push_back(image);
    }
    Mat blob;
    bool swapRB = false;
    cv::dnn::blobFromImages(images, blob, 1.0, cv::Size(224, 224), 128, swapRB, false);
    auto start = high_resolution_clock::now(); 
    for (size_t i=0; i<cnt_iterations; i++)
    {
        net.setInput(blob);
        Mat score = net.forward();
    }
    auto stop = high_resolution_clock::now();
    std::chrono::duration<double> seconds = (stop - start); 
    double elapsed_time = seconds.count();
    // std::cerr << score << "\n";
    fprintf(stderr, "time: \t%f\n", elapsed_time);
    fprintf(stderr, "ips: \t%f\n", static_cast<float>(cnt_iterations) * batch_size / elapsed_time);
    return 0;
}
